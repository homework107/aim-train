
const start = document.getElementById('start')
const screens = document.querySelectorAll('.screen')
const timeList = document.getElementById('time-list')
const board = document.getElementById('board')
let grd 
let time = 0
let score = 0
const timeEl = document.getElementById('timer')

const colors = [
    '#FF0000',
    '#00FFFF',
    '#7FFFD4',
    '#00BFFF',
    '#C0C0C0',
    '#778899',
    '#FFFF00',
    '#228B22'
]

function getRandomColor() {
    const colorRandom = Math.floor(Math.random() * colors.length)
    return colors[colorRandom]
}

function createLinearGradient(){
    grd.addColorStop(0,getRandomColor());
    grd.addColorStop(1,getRandomColor());
}

start.addEventListener('click', (event) => {
    event.preventDefault()
    screens[0].classList.add('up')
})

timeList.addEventListener('click', (event) => {
    if (event.target.classList.contains('time-btn')) {
        time = parseInt(event.target.getAttribute('data-time'))
        screens[1].classList.add('up')
        startGame()
    }
})

board.addEventListener('click', event => {
    if (event.target.classList.contains('circle')) {
        score++
        console.log(score)
        event.target.remove()
        createRandomCircle()
    }
})

function getRandomNumber(min, max) {
    return Math.floor(Math.random() * (max - min) + min)
}

function createRandomCircle() {
    const circle = document.createElement('div')
    const size = getRandomNumber(10, 60)
    const { width, height } = board.getBoundingClientRect()
    const y = getRandomNumber(0, height - size)
    const x = getRandomNumber(0, width - size)
    circle.classList.add('circle')
    circle.style.width = `${size}px`
    circle.style.height = `${size}px`
    circle.style.top = `${y}px`
    circle.style.left = `${x}px`
    circle.style.background = getRandomColor()

    board.append(circle)
}

function decraseTime() {
    console.log(123)
    if (time === 0) [
        finishGame()
    ]
    else {
        let current = --time
        if(current < 10){
            current = `0${current}`
        }
        setTime(current)
    }
}

function setTime(value) {
    console.log(321)
    timeEl.innerHTML = `00:${value}`
}

function startGame() {
    setInterval(decraseTime, 1000)
    createRandomCircle()
    setTime(time)
}

function finishGame(){
    timeEl.parentNode.classList.add('hide')
    board.innerHTML = `<h1>Ваш счёт: <span class = "primary">${score}</span></h1>`
}


// let name = prompt('Как звать вас сударь?')
// if (name == null){
//     alert('Вы ничего не ответили хам!!!!!')
// }
// let city = prompt('Из какого края вы сударь?')
// if (city == null){
//     alert('Вы ничего не ответили хам!!!!!')
// }
// alert('Приветствую вас '+name+' из края '+city)
// let vopr = confirm('Желаете ли вы отведать чего-либо  у нас?')
// if (vopr == true){
//     alert('Тогда прошу за мной.')
// }
// else{
//     alert('Ваше право.')
// }